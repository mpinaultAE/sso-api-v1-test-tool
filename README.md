**Test SSO and AE CRM v1 API with this utility**

This utility will take a Partner Id, Certificate, and PartnerUserId and allow for calls into AE CRM as long as those Partner (IDs) are stored in tblSSOPartners with the certificate thumbprint.  There are configurable values stored in the web.config which contain the initial values which may be overriden in the UI of the utility.

*Any questions, please follow up with Doug C. or Mark P. regarding the SSO/APIv1 utility.*

---

## SSO Test

1. Ensure a PartnerId and PartnerUserID are entered on the UI.  These initial values come from the web.config.
2. Last name and email are optional but desired.  The Saml response must include the attributes even if they are blank.
3. Ensure the web.config is configured to point to the proper environment and the configurables are set.
4. Click SSO To AdvisorEngineCRM

---

## API Test

1. On load, click the link "Call AdvisorEngine CRM API"
2. Ensure the UserID (PartnerUserID) is populated.  
3. Choose the pre-set API operation to run (several buttons to choose from).  Clicking the button will trigger the API call.
