﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AdvisorEngineCrmExample_ASP_SSO.APIHelpers;
using AdvisorEngineCrmExample_ASP_SSO.SSOHelpers;
using Jx.Framework.DTO.Alert;
using Jx.Framework.DTO.Base;
using Jx.Framework.DTO.Record;
using Jx.Framework.DTO.User;

namespace AdvisorEngineCrmExample_ASP_SSO
{
    public partial class API : System.Web.UI.Page
    {
        protected void cmdOK_Click(object sender, EventArgs e)
        {
            if (this.txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
                try
                {
	                var client = new HttpClient();
                    var jwt = new JwtTokenHelper();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13;

                    var response = Task.Run(() => client.GetAsync(SSOConfig.ApiRequestUrlBase + "/v1/record")).Result;

                    if (response.IsSuccessStatusCode)
                    {
	                    var data = response.Content.ReadAsAsync<JxDataList>().Result;
	                    lblResults.Text = "API response: Number of rows returned: " + data.ListData.Rows.Count + " out of " + data.ListInfo.TotalRows;
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {response.StatusCode}";
                }
                catch (Exception exception)
                {
                    Response.Write($"ERROR: {exception}");
                }
        }

        protected void cmdOK_RecDetail_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
                try
                {
                    var client = new HttpClient();
                    var jwt = new JwtTokenHelper();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = Task.Run(() => client.GetAsync($"{SSOConfig.ApiRequestUrlBase}/v1/record/b6417654-95a9-416b-8bdf-5c9e216ebcc9")).Result;

                    if (response.IsSuccessStatusCode) {
                        var data = response.Content.ReadAsAsync<RecordDetailAPI>().Result;
                        lblResults.Text = $"The record detail name is : {data.RecordDetail.Record_Name} which includes the following person " +
	                                           $"{data.RecordDetail.First_Name_Person_1} {data.RecordDetail.Last_Name_Person_1}";
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {response.StatusCode}";
                }
                catch (Exception exception)
                {
                    Response.Write($"ERROR: {exception}");
                }
        }

        protected void cmdOK_RecDetailPersons_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
            {
                try
                {
                    var client = new HttpClient();
                    var jwt = new JwtTokenHelper();

                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = Task.Run(() => client.GetAsync(SSOConfig.ApiRequestUrlBase + "/v1/record/b6417654-95a9-416b-8bdf-5c9e216ebcc9?span=Persons")).Result;

                    if (response.IsSuccessStatusCode) {
	                    var data = response.Content.ReadAsAsync<RecordDetailAPI>().Result;
	                    var persons = data.GetFromPayload<PersonsDTO>("Persons");

	                    var names = "There are no persons on this record";
	                    if (persons.Persons.Any())
		                    names = persons.Persons[0].Person.FirstName + " " + persons.Persons[0].Person.LastName;

	                    //For now, 2 persons is the limit to a Record, so we index [1]
	                    if (persons.Persons.Count() > 1)
		                    names += " && " + persons.Persons[1].Person.FirstName + " " + persons.Persons[1].Person.LastName;

	                    lblResults.Text = "On this record there are " + persons.Persons.Count() + " names = " + names;
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {response.StatusCode}";
                }
                catch (Exception exception)
                {
                    Response.Write($"ERROR: {exception}");
                }
            }
        }

        protected void cmdOK_RecDetailRecTotalAssetDetails_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
                try
                {
                    var client = new HttpClient();
                    var jwt = new JwtTokenHelper();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = Task.Run(() => client.GetAsync(SSOConfig.ApiRequestUrlBase + "/v1/record/b6417654-95a9-416b-8bdf-5c9e216ebcc9?span=RecordTotals,AssetDetailList")).Result;

                    if (response.IsSuccessStatusCode) {
	                    var data = response.Content.ReadAsAsync<RecordDetailAPI>().Result;

	                    var recTotals = data.GetFromPayload<RecordTotalsDTO>("RecordTotals");
	                    var busAssetListDetail = data.GetFromPayload<JxDataList>("RecordAssetDetail_Business");
	                    var finAssetListDetail = data.GetFromPayload<JxDataList>("RecordAssetDetail_FinancialAccount");
	                    var persPropAssetListDetail = data.GetFromPayload<JxDataList>("RecordAssetDetail_PersonalProperty");
	                    var realestAssetListDetail = data.GetFromPayload<JxDataList>("RecordAssetDetail_RealEstateProperty");
	                    var optionsAssetListDetail = data.GetFromPayload<JxDataList>("RecordAssetDetail_StockOption");

	                    lblResults.Text = "Record's net worth is " + recTotals.Net_Worth.ToString("C") + " with total assets of " + recTotals.Total_Assets.ToString("C");
	                    lblResults2.Text = "They currently have " + finAssetListDetail.AssetFinancialAccount.Count() + " Financial Accounts";
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {response.StatusCode}";
                }
                catch (Exception exception)
                {
	                Response.Write($"ERROR: {exception}");
                }
        }

        protected void cmdOK_GetListAssetsByRecord_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
                try
                {
                    var client = new HttpClient();
                    var jwt = new JwtTokenHelper();

                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = Task.Run(() => client.GetAsync(SSOConfig.ApiRequestUrlBase + "/v1/record/b6417654-95a9-416b-8bdf-5c9e216ebcc9/Asset")).Result;

                    if (response.IsSuccessStatusCode) {
	                    var data = response.Content.ReadAsAsync<JxDataList>().Result;
	                    lblResults.Text = "For this record there are " + data.ListData.Rows.Count + " returned records with a total of " + data.ListInfo.TotalRows + " assigned to this record";
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {response.StatusCode}";
                }
                catch (Exception exception)
                {
                    Response.Write("ERROR: " + exception.ToString());
                }
        }

        protected void cmdOK_AddAlert_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0)
                Response.Write("All fields are required.");
            else
                try
                {
                    // get current user
                    var client = new HttpClient();
                    var jwt = new JwtTokenHelper();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // You can get current user by /User.  
                    // You can get a list of users via /User?All=True
                    var client1 = client;
                    var userResponse = Task.Run(() => client1.GetAsync(SSOConfig.ApiRequestUrlBase + "/v1/User")).Result;

                    if (userResponse.IsSuccessStatusCode) {
	                    var userData = userResponse.Content.ReadAsAsync<List<UserDTO>>().Result;


	                    // Create alert structure
	                    var alert = new AlertAddDTO {
		                    alertSubject = "This is a test of adding api alert",
		                    alertDetail = "This is the body of the alert.  You can do a lot here including html",
		                    alertArea = "Assets",
		                    // You can put any area you want.  It describes to the user what this pertains too - ex: Asset, Action, Record, etc
		                    alertType = AlertType.SystemNotification,
		                    // This is the only one you should really be using.
		                    notificationLevel = NotificationLevel.Medium,
		                    // Low, Medium, High.  Where in the priority of the list its sorted on the UI
		                    alertOwnerIDs = new List<Guid>() { userData.First().UserID }
	                    };

	                    // Now add alert
	                    client = new HttpClient();
	                    jwt = new JwtTokenHelper();
	                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", jwt.CreateToken(Guid.Parse(this.txtPartnerUserID.Text)));
	                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

	                    var response = Task.Run(() => client.PutAsJsonAsync($"{SSOConfig.ApiRequestUrlBase}/v1/Alert", alert)).Result;

	                    lblResults.Text = response.IsSuccessStatusCode
		                    ? $"An alert to {userData.First().EmailAddress} has been added"
		                    : $"An alert failed to be added for {userData.First().EmailAddress}";
                    }
                    else
	                    lblResults.Text = $"API Status code was not successful: {userResponse.StatusCode}";
                }
                catch (Exception exception)
                {
                    Response.Write($"ERROR: {exception}");
                }
        }
    }
}