﻿using System.Configuration;

namespace AdvisorEngineCrmExample_ASP_SSO.SSOHelpers
{
    public static class SSOConfig
    {
         /// <summary> 
        /// This is for example only.  AdvisorEngine will provide you with your identifier.
        /// </summary>  
        // public static string PartnerID = "e60a04e7-7c71-4704-958a-e971364100be";
        public static string PartnerId => ConfigurationManager.AppSettings["PartnerId"];

         /// <summary> 
        /// AdvisorEngine CRM's SSO service and API URLs. This will change depending on server connecting to  
        /// Dev/QA: https://api-dev.junxure-cloud.net
        /// Production: https://www.junxurecloud.com
        /// </summary> 
        public static string SamlSsoRequestUrl = urlBase + "/SSO/SAMLSSORequest";
        public static string ApiRequestUrlBase = urlBase + "/api";
        //public const string urlBase = "http://localhost";

        public static string urlBase =>
	        ConfigurationManager.AppSettings["urlBase"] != null && ConfigurationManager.AppSettings["urlBase"] != string.Empty 
		        ? ConfigurationManager.AppSettings["urlBase"] 
		        : "https://api-dev.junxurecloud.net";
    }
}
