﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Xml;
using ComponentSpace.SAML2;
using ComponentSpace.SAML2.Assertions;
using ComponentSpace.SAML2.Protocols;

namespace AdvisorEngineCrmExample_ASP_SSO.SSOHelpers
{
    public class AdvisorEngineSamlResponse
    {
        private static X509Certificate2 _spCertificate;

        public XmlElement GetSamlAssertion(string idpUrl, Guid PartnerUserID, string PartnerUserLastName, string PartnerUserEmail)
        {
            var samlResponse = GetSamlResponse(idpUrl, PartnerUserID, PartnerUserLastName, PartnerUserEmail);
            var samlResponseXml = SignSamlResponse(samlResponse);
            return samlResponseXml;
        }

        private SAMLResponse GetSamlResponse(string idpUrl, Guid partnerUserId, string partnerUserLastName, string partnerUserEmail)
        {

            var now = DateTime.UtcNow;
            var notOnOrAfter = now.AddSeconds(120); //2 minute window for SSO expiration validity

            //AdvisorEngine CRM SSO URL
            string spUrl = $"{SSOConfig.SamlSsoRequestUrl}?PartnerID={SSOConfig.PartnerId}";

            var saml = new SAMLResponse
            {
	            Issuer = new Issuer(idpUrl),
	            Destination = spUrl,
	            Status = new Status(SAMLIdentifiers.PrimaryStatusCodes.Success, null)
            };

            var assertion = new SAMLAssertion
            {
	            Issuer = new Issuer(idpUrl),
	            Subject = SetAssertionSubject(notOnOrAfter, spUrl, partnerUserId),
	            Conditions = new Conditions(now, notOnOrAfter)
            };


            assertion.Conditions.ConditionsList.Add(SetAudienceRestriction(spUrl));
            assertion.Statements.Add(SetAuthnStatement());

            //Create the assertion attributes
            var attribStatement = new AttributeStatement();

            attribStatement.Attributes.Add(new SAMLAttribute("PartnerID", SAMLIdentifiers.AttributeNameFormats.Basic, "PartnerID", null, SSOConfig.PartnerId.ToString()));
            attribStatement.Attributes.Add(new SAMLAttribute("PartnerUserID", SAMLIdentifiers.AttributeNameFormats.Basic, "PartnerUserID", null, partnerUserId));
            attribStatement.Attributes.Add(new SAMLAttribute("PartnerUserLastName", SAMLIdentifiers.AttributeNameFormats.Basic, "PartnerUserLastName", null, partnerUserLastName));
            attribStatement.Attributes.Add(new SAMLAttribute("PartnerUserEmail", SAMLIdentifiers.AttributeNameFormats.Basic, "PartnerUserEmail", null, partnerUserEmail));
            assertion.Statements.Add(attribStatement);

            saml.Assertions.Add(assertion);

            return saml;
        }

        private static Subject SetAssertionSubject(DateTime notOnOrAfter, string spUrl, Guid PartnerUserID)
        {
            // NameID must be set to the userID
            var subject = new Subject(new NameID(PartnerUserID.ToString(), null, null, SAMLIdentifiers.AttributeNameFormats.Unspecified, null));
            var subjectConfirmation = new SubjectConfirmation(SAMLIdentifiers.SubjectConfirmationMethods.Bearer);
            var subjectConfirmationData = new SubjectConfirmationData { NotOnOrAfter = notOnOrAfter, Recipient = spUrl };

            subjectConfirmation.SubjectConfirmationData = subjectConfirmationData;
            subject.SubjectConfirmations.Add(subjectConfirmation);
            return subject;
        }
       
        private static AudienceRestriction SetAudienceRestriction(string spUrl)
        {
            var audienceRestriction = new AudienceRestriction();
            audienceRestriction.Audiences.Add(new Audience(spUrl));
            return audienceRestriction;
        }
        
        private AuthnStatement SetAuthnStatement()
			=> new AuthnStatement
	        {
		        SessionIndex = string.Empty,
		        AuthnContext = new AuthnContext { AuthnContextClassRef = new AuthnContextClassRef(SAMLIdentifiers.AuthnContextClasses.Unspecified) }
	        };

        private XmlElement SignSamlResponse(SAMLResponse samlResponse)
        {
            var samlResponseXml = samlResponse.ToXml();
            SAMLMessageSignature.Generate(samlResponseXml, Certificate.PrivateKey);
            return samlResponseXml;
        }

        public static X509Certificate2 Certificate
        {
            get
            {
	            if (_spCertificate?.PrivateKey != null)
		            return _spCertificate;

	            var x509Store = new X509Store("MY", StoreLocation.LocalMachine);
	            x509Store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
	            _spCertificate = x509Store.Certificates.OfType<X509Certificate2>().FirstOrDefault(c => c.Thumbprint == (ConfigurationManager.AppSettings["PartnerCertThumbprint"]).ToUpper());

	            return _spCertificate?.PrivateKey != null 
						   ? _spCertificate 
						   : TestCert;
            }
        }

		private static X509Certificate2 TestCert {
			get {
				//this cert will only work on https://api-dev.junxurecloud.net.  This is a test-only certificate using a test Partner set up in Dev/Qa
				String fileName = Path.Combine(HttpRuntime.AppDomainAppPath, "idp.pfx");
				string password = "password";
				return new X509Certificate2(fileName, password);
			}
		}
	}
}
