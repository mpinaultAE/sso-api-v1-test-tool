﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSO.aspx.cs" Inherits="AdvisorEngineCrmExample_ASP_SSO.SSO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AdvisorEngine CRM SAML SSO Example</title>
</head>
<body>
    <br />
    <h1>SAML SSO Example</h1>
    <p>
        This is a by user prerequisite for API authentication 
    </p>

    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td><asp:Label ID="lblPartnerID" runat="server" Text="PartnerID (Guid):"></asp:Label></td>
                <td><asp:TextBox ID="tbPartnerID" runat="server" Text="" Columns="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPartnerUserID" runat="server" Text="PartnerUserID (Guid):"></asp:Label></td>
                <td><asp:TextBox ID="txtPartnerUserID" runat="server" Text="" Columns="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPartnerUserLastName" runat="server" Text="User Last Name:"></asp:Label></td>
                <td><asp:TextBox ID="txtlPartnerUserLastName" runat="server" Columns="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPartnerUserEmail" runat="server" Text="User Email:"></asp:Label></td>
                <td><asp:TextBox ID="txtPartnerUserEmail" runat="server" Columns="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblRelayState" runat="server" Text="RelayState:"></asp:Label></td>
                <td><asp:TextBox ID="txtRelayState" runat="server" Text="" Columns="100"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <br />
                    <asp:Button ID="cmdOK" runat="server" Text="SSO To AdvisorEngine CRM" onclick="cmdOK_Click" />
                </td>               
            </tr>
        </table>               
    </div>
    </form>
    <br />
    <a href="API.aspx">Call AdvisorEngine CRM API</a>
</body>
</html>
