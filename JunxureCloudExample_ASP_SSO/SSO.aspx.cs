﻿using System;
using AdvisorEngineCrmExample_ASP_SSO.SSOHelpers;
using ComponentSpace.SAML2.Profiles.SSOBrowser;

namespace AdvisorEngineCrmExample_ASP_SSO
{
    public partial class SSO : System.Web.UI.Page
    {
	    protected void cmdOK_Click(object sender, EventArgs e)
        {
            if (txtPartnerUserID.Text.Trim().Length == 0) // || txtRelayState.Text.Trim().Length == 0)
					Response.Write("All fields are required.");
            else if (AdvisorEngineSamlResponse.Certificate?.PrivateKey == null)
                Response.Write("A public certificate is being referenced or there is no certificate installed at all.  Please install the private certificate referenced by the thumbprint.");
            else
            {
                try
                {
                    var assertUrl = $"{SSOConfig.SamlSsoRequestUrl}?PartnerID={SSOConfig.PartnerId}";
                    var saml = new AdvisorEngineSamlResponse();
                    var samlResponseXml = saml.GetSamlAssertion(CreateAbsoluteUrl("~/"), 
																Guid.Parse(txtPartnerUserID.Text), 
																txtlPartnerUserLastName.Text, 
																txtPartnerUserEmail.Text);
                    IdentityProvider.SendSAMLResponseByHTTPPost(Response, assertUrl, samlResponseXml, txtRelayState.Text);
                }
                catch (Exception exception)
                {
                    Response.Write(exception.ToString());
                }
            }
        }

        private string CreateAbsoluteUrl(string relativeUrl)
            => new Uri(Request.Url, ResolveUrl(relativeUrl)).ToString();

        public void Page_Load(object sender, EventArgs e)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["PartnerId"] != null &&
               System.Configuration.ConfigurationManager.AppSettings["PartnerId"] != string.Empty)
                tbPartnerID.Text = System.Configuration.ConfigurationManager.AppSettings["PartnerId"];

            if (System.Configuration.ConfigurationManager.AppSettings["PartnerUserID"] != null &&
                System.Configuration.ConfigurationManager.AppSettings["PartnerUserID"] != string.Empty)
					txtPartnerUserID.Text = System.Configuration.ConfigurationManager.AppSettings["PartnerUserID"];

            //Where do we want to redirect to?  Empty is main page, or a specific location such as below
            txtRelayState.Text = SSOConfig.urlBase + "/Tools/Workspace/Record";
        }
    }
}