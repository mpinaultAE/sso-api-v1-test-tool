﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="API.aspx.cs" Inherits="AdvisorEngineCrmExample_ASP_SSO.API" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AdvisorEngine CRM API Example</title>
</head>
<body>
    <br />
    <h1>API Example</h1>
    <p />
    <h3>To use this in the manner defined be sure to go to nuget and add the following items:</h3>
    <p><b> ===> Microsoft.AspNet.WebApi.Client </b></p>
    <p><b> ===> Newtonsoft.Json</b></p>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td><asp:Label ID="lblPartnerUserID" runat="server" Text="UserID (Guid):"></asp:Label></td>
                    <!-- MGP Test Account --><td><asp:TextBox ID="txtPartnerUserID" runat="server" Text="07EF2E75-2BE7-499E-A5C5-0D838074ABC4" Columns="50"></asp:TextBox></td>
                    <td><asp:Button ID="cmdOK" runat="server" Text="Call API - Record List" onclick="cmdOK_Click" /></td> 
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><asp:Button ID="cmdOK_RecDetail" runat="server" Text="Call API - Record Detail" onclick="cmdOK_RecDetail_Click" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><asp:Button ID="cmdOK_RecDetailPersons" runat="server" Text="Call API - Rec Detail + Persons" onclick="cmdOK_RecDetailPersons_Click" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><asp:Button ID="cmdOK_RecDetailRecTotalAssetDetails" runat="server" Text="Call API - Rec Detail + Totals + Detailed Asset List" onclick="cmdOK_RecDetailRecTotalAssetDetails_Click" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><asp:Button ID="cmdOK_GetListAssetsByRecord" runat="server" Text="Call API - Paged Asset List by Record Id" OnClick="cmdOK_GetListAssetsByRecord_Click" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><asp:Button ID="cmdOK_AddAlert" runat="server" Text="Call API - Add Alert 'Test is test' to current user" OnClick="cmdOK_AddAlert_Click"/></td>
                </tr>
                <tr>
                    <td colspan="3"><br /><asp:Label ID="lblResults" runat="server" Text=""></asp:Label></td>      
                </tr>
                <tr>
                    <td colspan="3"><br /><asp:Label ID="lblResults2" runat="server" Text=""></asp:Label></td>      
                </tr>
            </table> 
        </div>
    </form>
    <br />
    <a href="SSO.aspx">SSO To AdvisorEngine CRM</a>
</body>
</html>
