﻿using System;

namespace AdvisorEngineCrmExample_ASP_SSO.APIHelpers
{
    public class JwtToken
    {
        public string Signature { get; set; }
        public string Payload { get; set; }

    }
   
    public class JwtTokenClaim
    {
        public Guid PartnerID { get; set; }
        public string Audience { get; set; }
        public Guid PartnerUserID { get; set; }
        public DateTime NotOnOrAfter { get; set; }
        public Guid JWTID { get; set; }

    }
}