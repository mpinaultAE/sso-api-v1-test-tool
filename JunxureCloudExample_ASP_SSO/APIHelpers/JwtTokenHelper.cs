﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using AdvisorEngineCrmExample_ASP_SSO.SSOHelpers;

namespace AdvisorEngineCrmExample_ASP_SSO.APIHelpers
{
    public class JwtTokenHelper
    {
	    private X509Certificate2 _spCertificate;

        private static X509Certificate2 Cert
        {
            get
            {
                //certificate can be used for https://api-dev.junxurecloud.net only.  Test certificate
                //with PartnerId E60A04E7-7C71-4704-958A-E971364100BE, thumbprint A6A4AE4E0B378EC73678E5812690AF50E3EC3769
                //Otherwise use the preset settings sent in the example/web.config with your certificate's thumbprint
                var fileName = Path.Combine(HttpRuntime.AppDomainAppPath, "idp.pfx");
                return new X509Certificate2(fileName, "password");
            }
        }

        private X509Certificate2 DynamicCertificate {
	        get {
		        if (_spCertificate?.PrivateKey != null)
			        return _spCertificate;

		        var x509Store = new X509Store("MY", StoreLocation.LocalMachine);
		        x509Store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
		        _spCertificate = x509Store.Certificates.OfType<X509Certificate2>().FirstOrDefault(c => c.Thumbprint == (ConfigurationManager.AppSettings["PartnerCertThumbprint"]).ToUpper());

		        return _spCertificate?.PrivateKey != null
			        ? _spCertificate
			        : TestCert;
            }
        }

        private static X509Certificate2 TestCert {
	        get {
		        //this cert will only work on https://api-dev.junxurecloud.net.  This is a test-only certificate using a test Partner set up in Dev/Qa
		        String fileName = Path.Combine(HttpRuntime.AppDomainAppPath, "idp.pfx");
		        string password = "password";
		        return new X509Certificate2(fileName, password);
	        }
        }

        public string CreateToken(Guid partnerUserId)
        {
            var jwtTokenClaim = new JwtTokenClaim()
            {
                PartnerID = Guid.Parse(SSOConfig.PartnerId),
                PartnerUserID = partnerUserId,
                Audience = SSOConfig.ApiRequestUrlBase,
                NotOnOrAfter = DateTime.UtcNow.AddMinutes(10),
                JWTID = Guid.NewGuid()
            };

            var claim = new JavaScriptSerializer().Serialize(jwtTokenClaim);
			var jwtToken = new JwtToken {
				Payload = claim,
				Signature = SignToken(claim)
			};

			return new JavaScriptSerializer().Serialize(jwtToken);
        }

        private string SignToken(string stringToBeSigned)
        {
            var rsa = (RSACryptoServiceProvider)DynamicCertificate.PrivateKey;
            var rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);

            rsaFormatter.SetHashAlgorithm("SHA256");
            var shaHash = new SHA256Managed();
            var signedHashValue = rsaFormatter.CreateSignature(shaHash.ComputeHash(new UnicodeEncoding().GetBytes(stringToBeSigned)));

            return Convert.ToBase64String(signedHashValue);
        }
    }
}